package com.android.assignment.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.assignment.MyApplication;
import com.android.assignment.R;
import com.android.assignment.adapter.CustomPagerAdapter;
import com.android.assignment.adapter.ImagesAdapter;
import com.android.assignment.api.ApiClient;
import com.android.assignment.api.ApiInterface;
import com.android.assignment.model.ImageModel;
import com.android.assignment.util.NetworkCheck;
import com.android.assignment.util.XMLParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    ApiClient apiClient;

    private List<ImageModel> imageModels = new ArrayList<>();
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // check permission
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHavePermission()) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                loadImages();
            } else {
                loadImages();
            }
        }
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void loadImages() {
        // if online
        if (NetworkCheck.isOnline(this)) {
            getImagesFromServer();
        } else { // if offline
            try {
                // Create the Realm instance
                realm = MyApplication.getRealmInstance();

                imageModels = realm.where(ImageModel.class).findAll();
                updateViewPager(imageModels);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getImagesFromServer() {
        ApiInterface apiService = apiClient.getClient().create(ApiInterface.class);
        apiService.getCatImagesFromApi("xml", "50")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                        List<ImageModel> imageModels = response.body();
                        ResponseBody body = response.body();
                        try {
                            String stringXml = body.string();
                            XMLParser parser = new XMLParser();
                            Document doc = parser.getDomElement(stringXml); // getting DOM element

                            NodeList nl = doc.getElementsByTagName("image");

                            // looping through all item nodes <item>
                            for (int i = 0; i < nl.getLength(); i++) {
                                Element e = (Element) nl.item(i);
                                String url = parser.getValue(e, "url"); // url child value
                                String id = parser.getValue(e, "id"); // id child value
                                String source_url = parser.getValue(e, "source_url"); // source_url child value

                                ImageModel imageModel = new ImageModel();
                                imageModel.setId(id);
                                imageModel.setSource_url(source_url);
                                imageModel.setUrl(url);
                                imageModels.add(imageModel);
                            }


                            updateViewPager(imageModels);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.v(TAG, t.getMessage());
                    }
                });
    }

    private void updateViewPager(List<ImageModel> imageModels) {
        if (imageModels != null && imageModels.size() > 0) {
            viewPager.setAdapter(new CustomPagerAdapter(MainActivity.this, imageModels));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.view_pager:
                Toast.makeText(getApplicationContext(), "View Pager Selected", Toast.LENGTH_LONG).show();
                updateViewPager(imageModels);
                viewPager.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                return true;
            case R.id.grid_view:
                Toast.makeText(getApplicationContext(), "Grid View Selected", Toast.LENGTH_LONG).show();
                viewPager.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                updateListView("grid");
                return true;
            case R.id.list_view:
                Toast.makeText(getApplicationContext(), "List View Selected", Toast.LENGTH_LONG).show();
                viewPager.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                updateListView("list");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateListView(String input) {
        ImagesAdapter mAdapter = new ImagesAdapter(this, imageModels, input);

        if (input.equals("list")) {
            RecyclerView.LayoutManager listLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(listLayoutManager);
        } else {
            RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    loadImages();
                    // permission was granted, yay! Do the
                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
