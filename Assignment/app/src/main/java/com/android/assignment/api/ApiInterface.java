package com.android.assignment.api;

import com.android.assignment.model.ImageModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("api/images/get")
    Call<ResponseBody> getCatImagesFromApi(@Query("format") String format, @Query("results_per_page") String results_per_page);

}