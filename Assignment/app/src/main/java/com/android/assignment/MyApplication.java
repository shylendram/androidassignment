package com.android.assignment;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {

    private static Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

    }

    public static Realm getRealmInstance() {
        if (realm == null) {
            return realm = Realm.getDefaultInstance();
        }
        return realm;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        realm.close();
    }
}
