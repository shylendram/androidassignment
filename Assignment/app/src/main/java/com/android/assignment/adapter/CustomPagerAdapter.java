package com.android.assignment.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.assignment.MyApplication;
import com.android.assignment.R;
import com.android.assignment.model.ImageModel;
import com.android.assignment.util.NetworkCheck;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class CustomPagerAdapter extends PagerAdapter {

    private Context context;
    private List<ImageModel> imageModels;
    private ImageModel imageModel;
    private Realm realm;

    public CustomPagerAdapter(Context context, List<ImageModel> imageModels) {
        this.context = context;
        this.imageModels = imageModels;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        imageModel = imageModels.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup itemView = (ViewGroup) inflater.inflate(R.layout.image_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.loading);
        requestOptions.error(R.mipmap.ic_launcher);

        if (NetworkCheck.isOnline(context)) {
            Glide.with(context)
                    .load(imageModel.getUrl())
                    .apply(requestOptions)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            // log exception
                            Log.e("TAG", "Error loading image", e);
                            return false; // important to return false so the error placeholder can be placed
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            try {
                                Realm realm = Realm.getDefaultInstance();
                                ImageModel existingImage = realm.where(ImageModel.class).equalTo("id", imageModel.getId()).findFirst();
                                if (existingImage == null) {
                                    Bitmap anImage = ((BitmapDrawable) resource).getBitmap();
                                    saveImage(anImage);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return false;
                        }
                    })
                    .into(imageView);
        } else {

            Glide.with(context)
                    .load(new File(imageModel.getLocalUrl()))
                    .apply(requestOptions)
                    .into(imageView);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return imageModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        ButterKnife.bind(this, view);
        return view == object;
    }

    private void saveImage(Bitmap image) {
        final String savedImagePath;

        String imageFileName = "Cat" + imageModel.getId() + ".jpg";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + "/CatImages");
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File(storageDir, imageFileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();

                Log.v("saved: ", savedImagePath);

                // Create the Realm instance
                realm = MyApplication.getRealmInstance();
                try {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            ImageModel imageModel1 = new ImageModel();

                            imageModel1.setId(imageModel.getId());
                            imageModel1.setUrl(imageModel.getUrl());
                            imageModel1.setSource_url(imageModel.getSource_url());
                            imageModel1.setLocalUrl(savedImagePath);

                            realm.insertOrUpdate(imageModel1);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Add the image to the system gallery
//            galleryAddPic(savedImagePath);
//            Toast.makeText(context, "IMAGE SAVED", Toast.LENGTH_LONG).show();
        }
    }

    /*private void galleryAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }*/

}