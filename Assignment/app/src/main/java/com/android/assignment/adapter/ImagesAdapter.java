package com.android.assignment.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.assignment.R;
import com.android.assignment.model.ImageModel;
import com.android.assignment.util.NetworkCheck;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {

    private Context context;
    private List<ImageModel> imageModelList;
    private String input;

    public ImagesAdapter(Context context, List<ImageModel> imageModelList, String input) {
        this.context = context;
        this.imageModelList = imageModelList;
        this.input = input;
    }

    @NonNull
    @Override
    public ImagesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cutom_image_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesAdapter.MyViewHolder holder, int position) {
        ImageModel imageModel = imageModelList.get(position);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.loading);
        requestOptions.error(R.mipmap.ic_launcher);

        if (input.equals("list")) {
            holder.imageUrl.setVisibility(View.VISIBLE);
            if (NetworkCheck.isOnline(context)) {
                holder.imageUrl.setText(imageModel.getUrl());
            } else {
                holder.imageUrl.setText(imageModel.getLocalUrl());
            }
        } else {
            holder.imageUrl.setVisibility(View.GONE);
        }
        if (NetworkCheck.isOnline(context)) {
            Glide.with(context)
                    .load(imageModel.getUrl())
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else {
            Glide.with(context)
                    .load(new File(imageModel.getLocalUrl()))
                    .apply(requestOptions)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return imageModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view)
        ImageView imageView;
        @BindView(R.id.image_url)
        TextView imageUrl;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
